function Header(props) {
    return (
        <header>
            <section>
                <img src="/image/deco-top-left.png" alt="왼쪽 위 장식" />
                <div className="deco">
                    <div></div>
                </div>
                <article>
                    <div>&#123;</div>
                    <div className="title">
                        <h1>{props.titleDate}</h1>
                        <h2>길드원 리스트</h2>
                    </div>
                    <div>&#125;</div>
                </article>
                <div className="deco">
                    <div></div>
                </div>
                <img src="/image/deco-top-right.png" alt="오른쪽 위 장식" />
            </section>
        </header>
    );
}

export default Header;