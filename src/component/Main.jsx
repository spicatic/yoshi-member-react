import React, { useState } from "react";
import { Link } from "react-router-dom";
import '../style/main.css';

function Main(props){

  const [isActivePopup, activePopup] = useState(false);

  const objMember = props.objMain.objMember;
  const objMap = props.objMain.objMap;

  const arrNameMap = objMap.arrNameJson.map((nameJson)=>{
      const numNameMap = nameJson.replace("member", "").replace(".json", "")
      const urlNameMap = numNameMap.slice(0,4) + '-' + numNameMap.slice(4, 6) + '-' + numNameMap.slice(6,8);
      return urlNameMap;
    }
  );

  let indexMapSameUrl = arrNameMap.findIndex( (str) => str === props.objMain.objUser.userLocation);
  
  let classNamePrevLink = "";
  let classNameNextLink = "";

  let urlPrevSelector = "/" + arrNameMap[indexMapSameUrl+1]
  let urlNextSelector = "/"
  if(indexMapSameUrl === 0) {
    urlPrevSelector = "/"+arrNameMap[indexMapSameUrl+1]
    urlNextSelector = "/"
  } else if (indexMapSameUrl === -1) {
    urlPrevSelector = "/"+arrNameMap[indexMapSameUrl+2]
    urlNextSelector = "/"
    
  } else if(indexMapSameUrl === arrNameMap.length-1) {
    urlPrevSelector = "/"
    urlNextSelector = "/"+arrNameMap[arrNameMap.length-2]
  } else {
    urlPrevSelector = "/"+arrNameMap[indexMapSameUrl+1]
    urlNextSelector = "/"+arrNameMap[indexMapSameUrl-1]
  }
  
  if(urlNextSelector === "/"){
    classNameNextLink = "off";
  }
  if(urlPrevSelector === "/"){
    classNamePrevLink = "off";
  }


  

  const arrObjDate = objMap.arrNameJson.map((nameJson, index) => {
      const numNameJson = nameJson.replace("member", "").replace(".json", "");
      const urlNameJson = numNameJson.slice(0,4) + '-' + numNameJson.slice(4, 6) + '-' + numNameJson.slice(6,8);
      const dateNameJson = numNameJson.slice(0,4) + "년 " + numNameJson.slice(4, 6) + "월 " + numNameJson.slice(6,8) + "일";
      var classNameLi = "off";
      if (urlNameJson === props.objMain.objUser.userLocation){
        classNameLi = "on";
      }

      if(props.objMain.objUser.userLocation === undefined && index === 0) {
        classNameLi = "on";
      }
    
      return(
        {
          "urlNameJson":urlNameJson,
          "dateNameJson":dateNameJson,
          "classNameLi":classNameLi
        }
      )
  })
  const liListDate = arrObjDate.map((objDate, index) => 
    <li key={index} className={objDate.classNameLi}>
      <Link to={"/" + objDate.urlNameJson} >
        <time dateTime={objDate.urlNameJson}>
          {objDate.dateNameJson}
        </time>
      </Link>
    </li>
  )
  const liMemberAdd = objMember.memberAdd.map((nameMember, index) => 
    <li key={index}>
      {nameMember}
    </li> 
  );
  const liMemberOut = objMember.memberOut.map((nameMember, index) => 
    <li key={index}>
      {nameMember}
    </li> 
  );
  const liMember = objMember.member.map((nameMember, index) => 
    <li key={index}>
      {nameMember}
    </li>
  );
  
    return (
        <main className="member">
            <div className="deco">
                <div></div>
            </div>
            <section className="section-member">
                <nav className="selector-date">
                    <button onClick={() => {activePopup(true)}}>
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 16">
                        <path id="icon_menu" data-name="icon_menu" d="M1,16a1,1,0,1,1,0-2H18a1,1,0,1,1,0,2ZM1,9A1,1,0,1,1,1,7H18a1,1,0,1,1,0,2ZM1,2A1,1,0,0,1,1,0H18a1,1,0,1,1,0,2Z"/>
                      </svg>
                    </button>
                    <Link to={urlPrevSelector} className={classNamePrevLink}>
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.083 15.519">
                      <path id="icon_prev" data-name="icon_prev" d="M10,4.917a.983.983,0,0,0-.862.493L4.383,13.493A1,1,0,0,0,5.245,15h9.51a1,1,0,0,0,.862-1.507L10.862,5.41A.983.983,0,0,0,10,4.917m0-2A2.973,2.973,0,0,1,12.586,4.4l4.755,8.083A3,3,0,0,1,14.755,17H5.245a3,3,0,0,1-2.586-4.521L7.414,4.4A2.973,2.973,0,0,1,10,2.917Z" transform="translate(-2.917 17.759) rotate(-90)"/>
                      </svg>
                    </Link>
                    <time dateTime={objMember.url}>{props.objMain.objMember.titleKorean}</time>
                    <Link to={urlNextSelector} className={classNameNextLink}>
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.083 15.519">
                        <path id="icon_next" data-name="icon_next" d="M10,4.917a.983.983,0,0,0-.862.493L4.383,13.493A1,1,0,0,0,5.245,15h9.51a1,1,0,0,0,.862-1.507L10.862,5.41A.983.983,0,0,0,10,4.917m0-2A2.973,2.973,0,0,1,12.586,4.4l4.755,8.083A3,3,0,0,1,14.755,17H5.245a3,3,0,0,1-2.586-4.521L7.414,4.4A2.973,2.973,0,0,1,10,2.917Z" transform="translate(17 -2.241) rotate(90)"/>
                      </svg>
                    </Link>
                </nav>
                <article className="history">
                  {
                    objMember.lengthMemberAdd > 0
                    ?<div className="in">
                      <h2>새로운 길드원<span>{objMember.lengthMemberAdd}</span></h2>
                      <ul>
                        {liMemberAdd}
                      </ul>
                    </div>
                    : null
                  }
                  {
                    objMember.lengthMemberOut > 0
                    ?<div className="out">
                        <h2>나간 길드원<span>{objMember.lengthMemberOut}</span></h2>
                        <ul>
                          {liMemberOut}
                        </ul>
                    </div>
                    : null
                  }
                </article>
                <article className="member">
                  <h2>현재 길드원<span>{objMember.lengthMember}</span></h2>
                    <ul>
                      {liMember}
                    </ul>
                </article>
            </section>
            <div className="deco">
                <div></div>
            </div>
            <section className={"wrapper-popup-list-date" + (isActivePopup ? " on":"")}>
              <article>
                <ul>
                  {liListDate}
                </ul>
              </article>
              <div className="button-close" onClick={() => {activePopup(false)}}>
              </div>
            </section>
        </main>
    );
}

export default Main;
