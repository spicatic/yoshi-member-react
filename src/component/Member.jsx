import React, {Component} from "react";


import Header from './Header.jsx';
import Main from './Main.jsx';
import Footer from './Footer.jsx';

class Member extends Component {
    constructor () {
      super ();
      this.state = {
        objMap:{
          arrNameJson:[],
          nameLastestJson:""
        },
        objMember:{
          titleKorean:"",
          member:[],
          memberAdd:[],
          memberOut:[],
          lengthMember:0,
          lengthMemberAdd:0,
          lengthMemberOut:0
        },
        objUser:{
          userLocation:""
        }
      };
    }
    render () {
      return(
          <>
            <Header titleDate={this.state.objMember.titleKorean} />
            <Main objMain={this.state}/>
            <Footer />  
        </>
      )
    }
    
    componentDidMount () {
        fetch(this.props.urlHost + "/public/json/map/map.json")
        .then(res => res.json())
        .then(jsonMap => {
            let userLocation = "";
            if (!this.props.userLocation) {
                userLocation = jsonMap.nameLastestJson;
            } else {
                userLocation = "member" + this.props.userLocation.replaceAll("-", "") + ".json";
            }
            console.log(userLocation);
            this.setState({objMap: jsonMap, objUser:{userLocation:this.props.userLocation} })
            fetch(this.props.urlHost + "/public/json/member/" + userLocation)
            .then(res => res.json())
            .then(jsonMember => {
            this.setState({objMember: jsonMember})
            }).catch((error) => {
              window.location.href = "/error/404-not-found"
            });
        })
    }
    componentDidUpdate(prevProps) {
      if (this.props.userLocation !== prevProps.userLocation) {
        fetch(this.props.urlHost + "/public/json/map/map.json")
        .then(res => res.json())
        .then(jsonMap => {
            let userLocation = "";
            if (!this.props.userLocation) {
                userLocation = jsonMap.nameLastestJson;
            } else {
                userLocation = "member" + this.props.userLocation.replaceAll("-", "") + ".json";
            }
            console.log(userLocation);
            this.setState({objMap: jsonMap, objUser:{userLocation:this.props.userLocation} })
            fetch(this.props.urlHost + "/public/json/member/" + userLocation)
            .then(res => res.json())
            .then(jsonMember => {
            this.setState({objMember: jsonMember})
            }).catch((error) => {
              window.location.href = "/error/404-not-found"
          });
        })
      }
    }
  }

export default Member;