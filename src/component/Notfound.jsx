import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

function Notfound(){
    let [timeMovePage, setTimeMovePage] = useState(5);
    
    useEffect(()=>{
        const loop = setInterval(()=>{
            setTimeMovePage(prev => prev - 1);
            if (timeMovePage === 1) {
                window.location = "/";
            }
        }, 1000)
        return () => clearInterval(loop);
    });
    
    return (
        <main className="not-found">
            <section>
                <article>
                    <h2>404</h2>
                    <p>페이지를 찾을 수 없습니다.</p>
                    <p>{timeMovePage}초 후에 이동합니다.</p>
                    <Link to="/">메인페이지</Link>
                </article>
            </section>
        </main>
    );
}

export default Notfound;