function Footer(){
    return (
        <footer>
            <section>
                <img src="/image/deco-bottom-left.png" alt="왼쪽 아래 장식" />
                <div className="deco">
                    <div></div>
                </div>
                <img src="/image/deco-bottom-right.png" alt="오른쪽 아래 장식" />
            </section>
        </footer>
    );
}

export default Footer;