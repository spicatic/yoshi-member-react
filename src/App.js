import React from "react";
import Member from './component/Member.jsx';
import { useParams } from 'react-router-dom';

import './style/reset.css';
import './style/app.css';
import './style/header.css';
import './style/footer.css';

let modeDev = false;
let urlHost = "";

if(modeDev === true){
  urlHost = "http://44.203.71.163:9001"
  console.log("개발모드입니다." + urlHost);
} else {
  urlHost = ""
}

function App(){
  const { userLocation } = useParams();
  return(
    <>
      <Member urlHost={urlHost} userLocation={userLocation}/>
    </>
  )

  
}


export default App;
